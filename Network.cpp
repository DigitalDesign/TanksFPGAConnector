#include "Network.h"

static inline QByteArray IntToArray(qint32 source);

qint32 ArrayToInt(QByteArray source) {
  qint32 temp;
  QDataStream data(&source, QIODevice::ReadWrite);
  data >> temp;
  return temp;
}

Network::Network(QObject *parent)
    : QObject(parent), size_(0) {
  sendSocket_ = new QTcpSocket(this);
  QObject::connect(sendSocket_, SIGNAL(error(QAbstractSocket::SocketError)), this,
                   SLOT(error(QAbstractSocket::SocketError)));
  QObject::connect(sendSocket_, SIGNAL(readyRead()), this,
                   SLOT(dataRecievedFromSocket()));
}

bool Network::connectToHost(QString host, int port) {
  sendSocket_->connectToHost(host, port);
  bool wait = sendSocket_->waitForConnected();
  return wait;
}

bool Network::writeData(QString data) {
  QByteArray arr;
  arr.append(data);
  if (sendSocket_->state() == QAbstractSocket::ConnectedState) {
    sendSocket_->write(IntToArray(arr.size())); // write size of data
    sendSocket_->write(arr);                    // write the data itself
    return sendSocket_->waitForBytesWritten();
  } else
    return false;
}

void Network::error(QAbstractSocket::SocketError socketError) {
  switch (socketError) {
  case QAbstractSocket::RemoteHostClosedError:
    break;
  case QAbstractSocket::HostNotFoundError:
   qDebug()<<tr("The host was not found. Please check the "
                                "host name and port settings.");
    break;
  case QAbstractSocket::ConnectionRefusedError:
   qDebug()<<tr("The connection was refused by the peer");
    break;
  case QAbstractSocket::AddressInUseError:
     qDebug()<< tr("address in use");
  default:
    qDebug()<<tr("The following error occurred: %1.").arg(sendSocket_->errorString());
  }
}


void Network::dataRecievedFromSocket() {
    QTcpSocket *socket = static_cast<QTcpSocket *>(sender());
    qint32 size = size_;
    while (socket->bytesAvailable() > 0) {
      buffer_.append(socket->readAll());
      qDebug() << "Size of buffer is " << buffer_.size();
      while ( (buffer_.size() > 3 ) || (size!=0) ) {
        if (size == 0) {
            size = (buffer_.at(0) - '0') * 100000 + (buffer_.at(1) - '0') * 10000+
                (buffer_.at(2) - '0') * 1000 + (buffer_.at(3) - '0') * 100
                +(buffer_.at(4) - '0') * 10+(buffer_.at(5) - '0') * 1;
          size_ = size;
          buffer_.remove(0, 6);//!
          qDebug() << size<< " "<<buffer_.size();
        }
        if (size > 0 && buffer_.size() >= size) {
          QByteArray data = buffer_.mid(0, size);
          buffer_.remove(0, size);
          size = 0;
          size_ = size;

          //
          emit dataReceived(data);
        }
      }
    }
}


QByteArray IntToArray(qint32 source)
{
  QByteArray temp(6,'0');
  QString str=QString::number(source);
  int e=temp.size()-1;
  for(int i=str.size()-1;i>=0;i--){
      temp[e--]=str[i].toLatin1();
  }
  return temp;
}
