#include "GameMap.h"
#include <QDebug>
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/memorybuffer.h"
#include "templates.h"
using namespace rapidjson;
GameMap::GameMap()
{

}



void GameMap::handleNetWorkInput(QString data)
{

    qDebug()<<"Handling "<<data;

    //parse network input
    std::string command = data.toStdString();
    Document doc;
    doc.Parse(command.c_str());

    //get command type
    std::string commandType = doc["CommandType"].GetString();

    if (commandType == GAME_MAP) {
        if(doc["FloatArray"].Size() >6){
            //store tree positions
            for(int i=1;i< doc["FloatArray"].Size();i++){
                if(i%2==1){//pack them together
                    treesPos.push_back(std::make_pair(std::stof(doc["FloatArray"][i].GetString()),
                                       std::stof(doc["FloatArray"][i-1].GetString())));
                }
            }
            emit GameStart();
        }
        else{
            //new Tank pos
            qDebug()<<"Receiving new tanks postion ";
            tank1.x = std::stof(doc["FloatArray"][0].GetString());
            tank1.y = std::stof(doc["FloatArray"][1].GetString());
            tank1.r = std::stof(doc["FloatArray"][2].GetString());
            tank2.x = std::stof(doc["FloatArray"][3].GetString());
            tank2.y = std::stof(doc["FloatArray"][4].GetString());
            tank2.r = std::stof(doc["FloatArray"][5].GetString());


        }
    }
}
