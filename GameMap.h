#ifndef GAMEMAP_H
#define GAMEMAP_H

#include <qobject.h>
#include <QString>

struct Position{
    float x,y,r;
};

class GameMap : public QObject {
    Q_OBJECT

public:
    GameMap();
    std::vector<std::pair<float,float>> treesPos;
    Position tank1,tank2;


public slots:
    void handleNetWorkInput(QString data);
signals:
    void GameStart();
private:

};


#endif // GAMEMAP_H
