#include <QCoreApplication>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <iostream>
#include <string>
#include "Network.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/memorybuffer.h"
#include <random>
#include "templates.h"
#include "GameMap.h"

using namespace rapidjson;

GameMap map;
float move,turn,shot;
QTimer gameTimer;
Network* network;
QSerialPort port1,port2;

void play(){
    //dastresi be x y , r  2 tank

    /*
    //be ezaye har derakht
    for(int i=0;i<map.treesPos.size();i++){
        std::pair<float,float> tree = map.treesPos[i];
        //dastresi be x,y har derakht
        qDebug()<<tree.first<<" "<<tree.second;
    }
    */
    std::string input;
    if(port1.isOpen())
        input = port1.readAll().toStdString();
    if(input.size()>0){
        input.pop_back();
        if(input.size()>6)
            return;
        QStringList parts=QString::fromStdString(input).split(',');
        if(parts.size() ==3){
           // qDebug()<<parts[0]<<" "<<parts[1];
            move=parts[0].toFloat();
            turn=parts[1].toFloat();
            shot=parts[2].toFloat();
        }
    }
}


void GameLoop(){
    play();
    Document doc;
    doc.Parse(CommandTemplate.c_str());

    doc["FloatArray"].PushBack(move, doc.GetAllocator());
    doc["FloatArray"].PushBack(turn, doc.GetAllocator());
    doc["FloatArray"].PushBack(shot, doc.GetAllocator());

    StringBuffer buffer;
    Writer<StringBuffer> writer(buffer);
    doc.Accept(writer);
    network->writeData(QString::fromStdString(std::string(buffer.GetString())));
    //qDebug()<<"Sending "+QString::fromStdString(std::string(buffer.GetString()));
   // qDebug()<<"Sending ";
    gameTimer.stop();
    gameTimer.start();
}

void openSerialPorts(QSerialPort& port){
    QList<QSerialPortInfo> list = QSerialPortInfo::availablePorts();
    int i=0;
    for(auto& elem:list){
        qDebug()<<i++<<" "<<elem.portName();
    }
    qDebug()<<"Which port to connect ? ";
    int portIndex;
    std::cin>>portIndex;
    port.setPortName(list[portIndex].portName());
    port.setBaudRate(QSerialPort::Baud115200);
    port.setDataBits(QSerialPort::Data8);
    port.setParity(QSerialPort::NoParity);
    port.setStopBits(QSerialPort::OneStop);
    port.setFlowControl(QSerialPort::FlowControl::HardwareControl);
    port.open(QIODevice::ReadWrite);
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    network=new Network();

    gameTimer.setInterval(160);
    gameTimer.stop();


    if( network->connectToHost("127.0.0.1",3000))
        qDebug()<<"Connect successfully ! ";

    openSerialPorts(port1);
    network->writeData(GameRequestString);
    //   gameTimer.start();
    QObject::connect(network,
                     &Network::dataReceived, &map,&GameMap::handleNetWorkInput);
    QObject::connect(&map,&GameMap::GameStart,[&](){gameTimer.start();});

    QObject::connect(&gameTimer,&QTimer::timeout,GameLoop);

    return a.exec();
}
